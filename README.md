The Denver Nuggets Fan App 

A site where fans can come and check out information and Merch for their favorite team. They can view and purchase game tickets and merchandise from the team. Fans can also view the stats of the players for the season. Along with that they can view the list of this seasons scheduled games. Fans can also contact customer support to inquiry about purchasing tickets and problems with merchandise.

Features:
Purchase Merchandise and Game Tickets
View Player Statistics
View Player Details
View List of Scheduled Games
Contact Customer Support

Technologies Used:
Custom Aura Components
Apex
SLDS Styling
Lightning Components

Contributors: Esteban Lopez, Josh Oanca, Timothy Parker, Edmund Wong, Luis Zapata
