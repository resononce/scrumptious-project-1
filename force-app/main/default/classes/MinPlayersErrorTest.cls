@isTest
    
private class MinPlayersErrorTest {
    private static testmethod void ErrorSuccess(){
       Player__c [] pm = [SELECT Name, Id FROM Player__c WHERE Name = 'Paul Millsap'];
        try {
            delete pm;
        }
        catch(Exception e) {
            System.assert(e.getMessage().contains('NBA teams must carry a minimum of 13 players.'));
    }
}
}