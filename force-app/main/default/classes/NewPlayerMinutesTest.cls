@isTest

private class NewPlayerMinutesTest {
    @TestSetup
    static void Setup(){
    Player__c plr = new Player__c(Name = 'Damian Lillard');
    insert plr;
    }
    
    @isTest
    public static void EstMinutesSuccess(){
        Player__c plr = [SELECT Est_Minutes_Per_Game__c FROM Player__c WHERE Name='Damian Lillard'];
        System.assertEquals(0,plr.Est_Minutes_Per_Game__c);
    }
}