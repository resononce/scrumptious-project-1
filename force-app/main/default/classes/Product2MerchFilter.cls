public class Product2MerchFilter {
    public Product2MerchFilter(ApexPages.StandardSetController ssc){
        
    }
    
    public List<Product2> products { get{
        return [SELECT Name, description, family FROM Product2 WHERE family = 'Merchandise'];
    } set; }
    
    public PageReference newMerch(){
        return Page.Merch;
    }
}
