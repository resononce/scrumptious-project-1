public class Product2StdTicketExtension {
    
    public List<Product2> products { get{
        if(products==null){
            return [SELECT Id, Name, productcode, description, family FROM Product2 WHERE Name Like '%Ticket%'];
        }else{
            return products;
        }
    } set; }    
    
    public Product2StdTicketExtension(ApexPages.StandardSetController ssc){
        
    }    
    
    public PageReference newTicket(){
        return Page.Ticket;
    }
}