public class TicketStandardController {
	public Product2 products { get{
        if(products==null){
            return [SELECT Id, Name FROM Product2 WHERE Name = 'Ticket*'];
        }else{
            return products;
        }
    } set; }
    
    public PageReference newTicket(){
        return Page.Ticket;
    }

}