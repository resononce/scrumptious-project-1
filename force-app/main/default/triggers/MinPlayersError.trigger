trigger MinPlayersError on Player__c (before delete) {
    for (Player__c plyr : Trigger.old){
        Integer num = [SELECT Count() FROM Player__c];
        if(num <= 13){
            plyr.AddError('NBA teams must carry a minimum of 13 players.');
        }
    }

}