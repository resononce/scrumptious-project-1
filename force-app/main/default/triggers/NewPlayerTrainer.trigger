trigger NewPlayerTrainer on Player__c (after insert, after update) {
    List<Trainer__c> trainerList = new List<Trainer__c>();
    for (Player__c a : [SELECT Id,Name FROM Player__c WHERE Id IN :Trigger.New AND Id NOT IN (SELECT Player__c FROM Trainer__c)]){
        trainerList.add(new Trainer__c (Name=a.Name + ' Personal Trainer'));
    }

    if (trainerList.size()>0){
        insert trainerList;
    }
}